/* eslint-disable camelcase */
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { User } from '../../store/services/api/Api';
import { GetChargeColor } from '../../utils/GetChargeColor';
import { Icon } from '../_ui/Icon/Icon';
import style from './UserInfo.module.scss';
import cn from 'classnames';
import { DateParse } from '../../utils/DateParse';
import { ColorType } from '../../utils/ColorType';
import { useUserUpdate } from '../../store/services/api/user/useUserUpdate';

type UserInfoProps = {
  user: User;
};

const UserInfo: React.FC<UserInfoProps> = ({ user }) => {
  const [edit, setEdit] = React.useState(false);
  const [username, setUsername] = React.useState(
    user.first_name + ' ' + user.last_name,
  );
  const [error, setError] = React.useState('');
  const navigate = useNavigate();
  const { mutate, isLoading } = useUserUpdate(user.id);

  const checkName = (name: string) => {
    return name.split(' ').length === 2 && name.length < 50;
  };

  return (
    <div className={style.component}>
      <div className={style.user_name_block}>
        <button
          onClick={() => {
            navigate('/');
          }}
          className={style.go_back}
        >
          <Icon name='keyboard_arrow_left' width={24} height={24}></Icon>
          <p>к списку пользователей</p>
        </button>
        <div className={cn(style.user_name, { [style.edit]: edit })}>
          {user.device?.is_active ? (
            <Icon name='outline' width={24} height={24}></Icon>
          ) : (
            <Icon name='outline_gray' width={24} height={24}></Icon>
          )}

          {!edit ? (
            <p>{user.first_name + ' ' + user.last_name}</p>
          ) : (
            <div className={style.input_container}>
              <input
                type='text'
                value={username}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
              <button disabled={isLoading} className={style.done}>
                <Icon
                  name='done'
                  width={24}
                  height={24}
                  onClick={() => {
                    if (checkName(username)) {
                      mutate(
                        {
                          id: user.id,
                          user: {
                            ...user,
                            first_name: username.split(' ')[0],
                            last_name: username.split(' ')[1],
                          },
                        },
                        {
                          onError: () => {
                            setError('Что-то пошло не так =(');
                            setEdit(false);
                          },
                          onSuccess: () => setEdit(false),
                        },
                      );
                    } else {
                      setEdit(false);
                      setError('Введите имя и фамилию');
                      setUsername(user.first_name + ' ' + user.last_name);
                    }
                  }}
                ></Icon>
              </button>
              <button disabled={isLoading} className={style.clear}>
                <Icon
                  name='clear'
                  width={24}
                  height={24}
                  onClick={() => {
                    setEdit(false);
                    setUsername(user.first_name + ' ' + user.last_name);
                  }}
                ></Icon>
              </button>
              {isLoading && <Icon name='loading' width={24} height={24}></Icon>}
            </div>
          )}
          {!edit && (
            <>
              <button className={style.pencil}>
                <Icon
                  name='pencil'
                  width={24}
                  height={24}
                  onClick={() => {
                    setError('');
                    setEdit(true);
                  }}
                ></Icon>
              </button>
              {error !== '' && <p className={style.error}>{error}</p>}
            </>
          )}
        </div>
      </div>
      {user.measurements[0] ? (
        <div className={style.user_data_block}>
          <p>Обновлено {DateParse(user.measurements[0].measured_at)}</p>
          <div className={style.device_info_block}>
            <div className={style.device_info}>
              <Icon name='tap_and_play' width={24} height={24}></Icon>
              <div className={style.line}></div>
              {user.device?.is_active ? (
                <Icon name='bluetooth_searching' width={24} height={24}></Icon>
              ) : (
                <Icon name='bluetooth_disable' width={24} height={24}></Icon>
              )}
              {user.device ? (
                <>
                  <div className={style.line}></div>
                  <svg
                    width={24}
                    height={24}
                    viewBox='0 0 24 24'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      d='M20 15.537V14.7C20 14.1477 20.4477 13.7 21 13.7V13.7C21.5523 13.7 22 13.2523 22 12.7V10.3C22 9.74772 21.5523 9.3 21 9.3V9.3C20.4477 9.3 20 8.85228 20 8.3V7.463C20 6.66 19.4 6 18.67 6L3.34 6C2.6 6 2 6.66 2 7.463V15.526C2 16.34 2.6 17 3.33 17L18.67 17C19.4 17 20 16.34 20 15.537Z'
                      stroke={GetChargeColor(user.device.charge)}
                      strokeWidth='2'
                    />
                    <rect
                      x='4'
                      y='8'
                      width={(14 * user.device.charge) / 100}
                      height='7'
                      rx='1'
                      fill={GetChargeColor(user.device.charge)}
                    />
                  </svg>
                  <p>{user.device.charge}%</p>
                </>
              ) : null}
            </div>
            <div
              className={cn(
                style.temperature,
                style[`${ColorType(user.last_temperature || undefined)}`],
              )}
            >
              {user.last_temperature}°C
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default UserInfo;
