import { FC } from 'react';

interface Props {
  name: string;
  height: number | string;
  width: number | string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [x: string]: any;
}

export const Icon: FC<Props> = ({ name, height, width, ...rest }) => {
  return (
    <svg
      version='1.1'
      xmlns='http://www.w3.org/2000/svg'
      style={{
        width: `${width}px`,
        height: `${height}px`,
        fill: 'currentcolor',
      }}
      {...rest}
    >
      <use xlinkHref={`/sprite.svg#${name}`} />
    </svg>
  );
};
