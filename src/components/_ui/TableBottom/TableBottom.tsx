import React from 'react';
import { User } from '../../../store/services/api/Api';
import { GetFilters } from '../../../utils/GetFilters';
import { Icon } from '../Icon/Icon';
import style from './TableBottom.module.scss';

type TableBottomProps = {
  filters: {
    normal: boolean;
    high: boolean;
    low: boolean;
  };
  setFilters: (data: { normal: boolean; high: boolean; low: boolean }) => void;
  active: User[];
  setActive: (data: User[]) => void;
  page: number;
  setPage: (data: number) => void;
  count: number;
  isPreviousData: boolean;
  hasMore: boolean;
};

const TableBottom: React.FC<TableBottomProps> = ({
  filters,
  setFilters,
  active,
  setActive,
  page,
  setPage,
  count,
  isPreviousData,
  hasMore,
}) => {
  return (
    <div className={style.component}>
      <div className={style.controls}>
        {filters.normal || filters.high || filters.low ? (
          <>
            <p>Фильтр: {GetFilters(filters)}</p>{' '}
            <p
              onClick={() =>
                setFilters({ normal: false, high: false, low: false })
              }
              className={style.link}
            >
              Сбросить фильтр
            </p>
          </>
        ) : null}
        {active.length !== 0 ? (
          <>
            <p>
              {active.length === 1 ? 'Выделен' : 'Выделено'}{' '}
              <b>{active.length}</b>{' '}
              {active.length === 1 ? 'пользователь' : 'пользователей'}
            </p>{' '}
            <p onClick={() => setActive([])} className={style.link}>
              Сбросить
            </p>
          </>
        ) : null}
      </div>
      <div className={style.pagination}>
        <p>
          {page * 10 + 1}-{Math.min(count, (page + 1) * 10)}/{count}
        </p>
        <button
          disabled={page === 0}
          onClick={() => setPage(Math.max(page - 1, 0))}
        >
          <Icon name='keyboard_arrow_left_gray' width={24} height={24}></Icon>
        </button>
        <button
          disabled={isPreviousData || !hasMore}
          onClick={() => {
            if (!isPreviousData && hasMore) {
              setPage(page + 1);
            }
          }}
        >
          <Icon name='keyboard_arrow_right' width={24} height={24}></Icon>
        </button>
      </div>
    </div>
  );
};

export default TableBottom;
