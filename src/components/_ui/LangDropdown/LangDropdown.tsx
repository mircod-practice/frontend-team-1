import React, { FC, useState } from 'react';
import style from './LangDropdown.module.scss';
import { Icon } from '../Icon/Icon';

interface LangDropdownProps {
  languages?: string[];
}

export const LangDropdown: FC<LangDropdownProps> = ({
  languages = ['RU', 'EN'],
}) => {
  const [isActive, setIsActive] = useState(false);
  const [selected, setSelected] = useState('RU');

  return (
    <div className={style.component}>
      <div className={style.container}>
        <button
          className={style.dropdownBtn}
          onClick={() => setIsActive(!isActive)}
        >
          {selected}
          <Icon width={24} height={24} name='keyboard_arrow_down'></Icon>
        </button>
        {isActive && (
          <div className={style.dropdownContent}>
            {languages.map((option) =>
              option !== selected ? (
                <div key={option} className={style.dropdownItem}>
                  <div className={style.line} />
                  <button
                    onClick={() => {
                      setSelected(option);
                      setIsActive(false);
                    }}
                    className={style.dropdownBtn}
                  >
                    {option}
                  </button>
                </div>
              ) : null,
            )}
          </div>
        )}
      </div>
    </div>
  );
};
