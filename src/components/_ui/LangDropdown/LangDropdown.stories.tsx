import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { LangDropdown } from './LangDropdown';

export default {
  title: 'LangDropdown',
  component: LangDropdown,

  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof LangDropdown>;

const Template: ComponentStory<typeof LangDropdown> = (args) => (
  <LangDropdown {...args} />
);

export const Primary = Template.bind({});

Primary.args = {
  // primary: true,
  // label: 'LangDropdown',
};
