import React from 'react';
import ContentLoader from 'react-content-loader';
import style from './Search.module.scss';

const SearchSkeleton = () => (
  <div className={style.searchResults}>
    <ContentLoader
      speed={2}
      width={388}
      height={88}
      viewBox='0 0 388 88'
      backgroundColor='#f3f3f3'
      foregroundColor='#ecebeb'
    >
      <rect x='0' y='88' rx='3' ry='3' width='178' height='6' />
      <rect x='0' y='0' rx='0' ry='0' width='388' height='24' />
      <rect x='0' y='32' rx='0' ry='0' width='388' height='24' />
      <rect x='0' y='64' rx='0' ry='0' width='388' height='24' />
    </ContentLoader>
  </div>
);

export default SearchSkeleton;
