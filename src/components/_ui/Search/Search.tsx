import { FC, useEffect, useState } from 'react';
import style from './Search.module.scss';
import { Icon } from '../Icon/Icon';
import cn from 'classnames';
import { User } from '../../../store/services/api/Api';
import SearchSkeleton from './SearchSkeleton';

type SearchProps = {
  placeholder: string;
  data: User[] | undefined;
  setSearchValue: (data: string) => void;
  isLoading: boolean;
};

export const Search: FC<SearchProps> = ({
  placeholder = 'Поиск',
  data,
  setSearchValue,
  isLoading,
}) => {
  const [display, setDisplay] = useState(false);
  const [wordEntered, setWordEntered] = useState('');
  const [showSkeleton, setShowSkeleton] = useState(false);

  useEffect(() => {
    wordEntered !== '' ? setShowSkeleton(true) : setShowSkeleton(false);
    const delayDebounceFn = setTimeout(() => {
      setSearchValue(wordEntered);
      setTimeout(() => {
        setShowSkeleton(false);
      }, 200);
    }, 500);
    return () => clearTimeout(delayDebounceFn);
  }, [wordEntered]);

  const setName = (name: string) => {
    setWordEntered(name);
    setDisplay(false);
    setSearchValue(name);
  };

  const clearInput = () => {
    setWordEntered('');
  };

  return (
    <div className={style.component}>
      <div className={style.searchInput}>
        <div className={style.searchIcon}>
          <Icon width={24} height={24} name='search'></Icon>
        </div>
        <input
          type='text'
          placeholder={placeholder}
          value={wordEntered}
          onFocus={() => setDisplay(true)}
          onBlur={(e) => {
            if (
              document.getElementsByClassName(style.searchResults).length !==
                0 &&
              !document
                .getElementsByClassName(style.searchResults)[0]
                .contains(e.relatedTarget)
            ) {
              setDisplay(false);
            }
          }}
          onChange={(e) => {
            if (e.target.value !== '') {
              setDisplay(true);
            }
            setWordEntered(e.target.value);
          }}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              setDisplay(false);
              setSearchValue(wordEntered);
            }
          }}
        />
        <div className={style.clearIcon}>
          {wordEntered === '' ? (
            <></>
          ) : (
            <Icon
              width={24}
              height={24}
              name='clear'
              onClick={clearInput}
            ></Icon>
          )}
        </div>
      </div>

      {display && (isLoading || showSkeleton) ? (
        <SearchSkeleton />
      ) : (
        display &&
        wordEntered !== '' &&
        !isLoading && (
          <div className={style.searchResults}>
            {data?.length === 0 ? (
              <button
                className={cn(style.searchResult, style.not_found)}
                onClick={() => setDisplay(false)}
              >
                Not found
              </button>
            ) : (
              data?.map((value) => {
                return (
                  <button
                    key={value.id}
                    className={style.searchResult}
                    onClick={() => {
                      setName(value.first_name || '' + value.last_name || '');
                    }}
                  >
                    {value.first_name || '' + value.last_name || ''}
                  </button>
                );
              })
            )}
          </div>
        )
      )}
    </div>
  );
};
