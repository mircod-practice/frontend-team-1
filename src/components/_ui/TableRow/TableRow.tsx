import React from 'react';
import { Icon } from '../Icon/Icon';
import style from './TableRow.module.scss';
import cn from 'classnames';
import { User } from '../../../store/services/api/Api';
import { Skeleton } from './Skeleton';
import { ColorType } from '../../../utils/ColorType';
import { DateParse } from '../../../utils/DateParse';
import { GetChargeColor } from '../../../utils/GetChargeColor';
import { Link } from 'react-router-dom';

type TableRowProps = {
  active: User[];
  setActive: (data: User[]) => void;
  user: User | null;
  id: number;
  isLoading: boolean;
  checkedAll: boolean;
};

const TableRow: React.FC<TableRowProps> = ({
  user,
  active,
  setActive,
  id,
  isLoading,
  checkedAll,
}) => {
  const [checked, setChecked] = React.useState(false);

  React.useEffect(() => {
    checkedAll ? setChecked(checkedAll) : null;
  }, [checkedAll]);

  React.useEffect(() => {
    if (active.length === 0) {
      setChecked(false);
    }
  }, [active]);

  if (isLoading) {
    return (
      <tr className={style.component}>
        <td className={style.skeleton}>
          <Skeleton width={24} height={24} />
        </td>
        <td className={style.skeleton}>
          <Skeleton width={373} height={24} />
        </td>
        <td className={style.skeleton}>
          <Skeleton width={305} height={24} />
        </td>
        <td className={style.skeleton}>
          <Skeleton width={162} height={24} />
        </td>
        <td className={style.skeleton}>
          <Skeleton width={95} height={24} />
        </td>
        <td>
          <Icon name='check_box_outline_blank' width={24} height={24}></Icon>
        </td>
      </tr>
    );
  }

  return (
    <tr className={cn(style.component, { [style.checked]: checked })}>
      <td>{id + 1}</td>
      <td>
        <Link to={`/user/${user?.id}`}>
          {user?.first_name} {user?.last_name}
        </Link>
      </td>
      {user?.last_temperature ? (
        <td className={cn(style[`${ColorType(user?.last_temperature)}`])}>
          <p className={cn(style[`${ColorType(user?.last_temperature)}`])}>
            {user?.last_temperature}°C
          </p>{' '}
          ({DateParse(user?.measurements[0].measured_at)})
        </td>
      ) : (
        <td className={style.no_data}>нет данных</td>
      )}
      {user?.device ? (
        <>
          <td>{user.device.uuid}</td>
          <td>
            <span>
              <svg
                width={24}
                height={24}
                viewBox='0 0 24 24'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
              >
                <path
                  d='M20 15.537V14.7C20 14.1477 20.4477 13.7 21 13.7V13.7C21.5523 13.7 22 13.2523 22 12.7V10.3C22 9.74772 21.5523 9.3 21 9.3V9.3C20.4477 9.3 20 8.85228 20 8.3V7.463C20 6.66 19.4 6 18.67 6L3.34 6C2.6 6 2 6.66 2 7.463V15.526C2 16.34 2.6 17 3.33 17L18.67 17C19.4 17 20 16.34 20 15.537Z'
                  stroke={GetChargeColor(user.device.charge)}
                  strokeWidth='2'
                />
                <rect
                  x='4'
                  y='8'
                  width={(14 * user.device.charge) / 100}
                  height='7'
                  rx='1'
                  fill={GetChargeColor(user.device.charge)}
                />
              </svg>
            </span>
            {user.device.charge}%
          </td>
        </>
      ) : (
        <>
          <td>
            <div className={style.dash} />
          </td>
          <td>
            <div className={style.dash} />
          </td>
        </>
      )}
      <td className={cn({ [style.checked]: checked })}>
        {!checked ? (
          <Icon
            name='check_box_outline_blank'
            width={24}
            height={24}
            onClick={() => {
              setChecked(true);
              user ? setActive([...active, user]) : null;
            }}
          ></Icon>
        ) : (
          <Icon
            name='check_box'
            width={24}
            height={24}
            onClick={() => {
              setChecked(false);
              user
                ? setActive([...active.filter((u) => user.id !== u.id)])
                : null;
            }}
          ></Icon>
        )}
      </td>
    </tr>
  );
};

export default TableRow;
