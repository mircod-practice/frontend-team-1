import React from 'react';
import ContentLoader from 'react-content-loader';

type SkeletonProps = {
  width: number;
  height: number;
};

export const Skeleton: React.FC<SkeletonProps> = ({ width, height }) => (
  <ContentLoader
    speed={2}
    width={width}
    height={height}
    viewBox={`0 0 ${width} ${height}`}
    backgroundColor='#f3f3f3'
    foregroundColor='#ecebeb'
  >
    <rect x='0' y='0' rx='0' ry='0' width={width} height={height} />
  </ContentLoader>
);
