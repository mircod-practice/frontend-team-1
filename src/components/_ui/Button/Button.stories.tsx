import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { Button } from './Button';

export default {
  title: 'Button',
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Login = Template.bind({});

Login.args = {
  buttonType: 'login',
  loading: false,
  disabled: false,
};

export const Logout = Template.bind({});

Logout.args = {
  buttonType: 'logout',
};

export const GoBack = Template.bind({});

GoBack.args = {
  buttonType: 'go_back',
};

export const Delete = Template.bind({});

Delete.args = {
  buttonType: 'delete',
  active: false,
};
