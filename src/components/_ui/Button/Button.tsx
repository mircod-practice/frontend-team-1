import cn from 'classnames';
import { FC, ReactNode } from 'react';
import style from './Button.module.scss';

interface ButtonProps {
  // передаем тип кнопки: login, logout, go_back, delete
  // active для чекбокса
  buttonType: string;
  loading?: boolean;
  disabled?: boolean;
  active?: boolean;
  children: ReactNode;
  onClick: () => void;
}
// передаем children
export const Button: FC<ButtonProps> = ({ ...props }: ButtonProps) => {
  const {
    children,
    buttonType,
    loading = false,
    active = false,
    disabled = false,
    onClick,
  } = props;
  return (
    <button
      type={buttonType === 'login' ? 'submit' : 'button'}
      disabled={disabled}
      className={cn(style.component, style[`${buttonType}`], {
        [style.load]: loading,
        [style.active]: active,
      })}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
