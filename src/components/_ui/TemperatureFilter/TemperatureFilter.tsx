import React, { FC, useState, useRef } from 'react';
import { Icon } from '../Icon/Icon';
import style from './TemperatureFilter.module.scss';
import cn from 'classnames';

type TemperatureFilterProps = {
  filters: {
    normal: boolean;
    high: boolean;
    low: boolean;
  };
  setFilters: (data: { normal: boolean; high: boolean; low: boolean }) => void;
};

export const TemperatureFilter: FC<TemperatureFilterProps> = ({
  filters,
  setFilters,
}) => {
  const [isActive, setIsActive] = useState(false);
  const ref = useRef<HTMLDivElement>(null);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleButtonClick = (e: any) => {
    if (ref.current && !ref.current.contains(e.target)) {
      setIsActive(false);
    }
  };
  React.useEffect(() => {
    document.addEventListener('click', handleButtonClick);

    return () => {
      document.removeEventListener('click', handleButtonClick);
    };
  }, []);

  return (
    <div className={style.component} ref={ref}>
      <button
        className={cn(style.dropdownBtn, { [style.active]: isActive })}
        onClick={() => setIsActive(!isActive)}
      >
        <Icon width={24} height={24} name='filter_alt'></Icon>
      </button>
      {isActive && (
        <div className={style.dropdownContent}>
          <p>Фильтровать: </p>
          <div className={style.line} />

          <div className={style.dropdownItem}>
            <button
              className={cn(style.checkboxButton, {
                [style.checked]: filters.normal,
              })}
              onClick={() => {
                setFilters({ ...filters, normal: !filters.normal });
              }}
            >
              {!filters.normal ? (
                <Icon
                  name={'check_box_outline_blank'}
                  height={16}
                  width={16}
                ></Icon>
              ) : (
                <Icon name={'check_box'} height={16} width={16}></Icon>
              )}
            </button>
            <p>Нормальная</p>
          </div>
          <div className={style.dropdownItem}>
            <button
              className={cn(style.checkboxButton, {
                [style.checked]: filters.high,
              })}
              onClick={() => {
                setFilters({ ...filters, high: !filters.high });
              }}
            >
              {!filters.high ? (
                <Icon
                  name={'check_box_outline_blank'}
                  height={16}
                  width={16}
                ></Icon>
              ) : (
                <Icon name={'check_box'} height={16} width={16}></Icon>
              )}
            </button>
            <p>Превышена</p>
          </div>
          <div className={style.dropdownItem}>
            <button
              className={cn(style.checkboxButton, {
                [style.checked]: filters.low,
              })}
              onClick={() => {
                setFilters({ ...filters, low: !filters.low });
              }}
            >
              {!filters.low ? (
                <Icon
                  name={'check_box_outline_blank'}
                  height={16}
                  width={16}
                ></Icon>
              ) : (
                <Icon name={'check_box'} height={16} width={16}></Icon>
              )}
            </button>
            <p>Понижена</p>
          </div>
        </div>
      )}
    </div>
  );
};
