import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { TemperatureFilter } from './TemperatureFilter';

export default {
  title: 'TemperatureFilter',
  component: TemperatureFilter,

  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof TemperatureFilter>;

const Template: ComponentStory<typeof TemperatureFilter> = (args) => (
  <TemperatureFilter {...args} />
);

export const Primary = Template.bind({});

Primary.args = {};
