import React from 'react';
import logo from '../../assets/logo_full.svg';
import { Button } from '../_ui/Button/Button';
import { Icon } from '../_ui/Icon/Icon';
import { Search } from '../_ui/Search/Search';
import style from './ControlPanel.module.scss';
import cn from 'classnames';
import { User } from '../../store/services/api/Api';
import ModalDelete from '../ModalDelete/ModalDelete';

type ControlPanelProps = {
  active: User[];
  setActive: (data: User[]) => void;
  setSearchValue: (data: string) => void;
  data: User[] | undefined;
  isLoading: boolean;
};

const ControlPanel: React.FC<ControlPanelProps> = ({
  active,
  setActive,
  setSearchValue,
  data,
  isLoading,
}) => {
  const [isOpen, setIsOpen] = React.useState(false);

  return (
    <div className={cn(style.component)}>
      <img src={logo} alt='logo_full' />

      <div className={cn(style.panel)}>
        <Search
          placeholder={'Поиск'}
          data={data}
          isLoading={isLoading}
          setSearchValue={setSearchValue}
        />

        <div>
          <Button
            buttonType='delete'
            onClick={() => {
              setIsOpen(!isOpen);
            }}
            disabled={!active.length}
            active={!!active.length}
          >
            <Icon width={24} height={24} name='delete' />
          </Button>
        </div>
      </div>
      <ModalDelete
        users={active}
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        setActive={setActive}
      />
    </div>
  );
};

export default ControlPanel;
