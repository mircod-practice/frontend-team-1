import React from 'react';
import NoResults from '../../assets/no_results.png';
import style from './NoMeasurementsFound.module.scss';

const NoMeasurementsFound = () => {
  return (
    <div className={style.component}>
      <img src={NoResults} alt='No results' />
      <p>Нет данных за этот день</p>
    </div>
  );
};

export default NoMeasurementsFound;
