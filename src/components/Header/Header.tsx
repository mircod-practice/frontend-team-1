import storage from 'local-storage-fallback';
import React, { memo } from 'react';
import { AuthContext } from '../../store/AuthContext/AuthContext';
import { useLogout } from '../../store/services/api/auth/useLogout';
import { Button } from '../_ui/Button/Button';
import { Icon } from '../_ui/Icon/Icon';
import { LangDropdown } from '../_ui/LangDropdown/LangDropdown';
import { useNavigate } from 'react-router-dom';
import style from './Header.module.scss';

const Header: React.FC = memo(() => {
  const { logged, setLogged } = React.useContext(AuthContext);
  const { mutate } = useLogout();
  const navigate = useNavigate();

  return (
    <header className={style.component}>
      <div className={style.content}>
        <h1>TEMPCOD</h1>
        <div className={style.controls}>
          <LangDropdown />
          {logged && (
            <Button
              buttonType='logout'
              onClick={() => {
                if (
                  storage.getItem('refresh_token') &&
                  storage.getItem('refresh_token') !== ''
                ) {
                  mutate(storage.getItem('refresh_token') || '', {
                    onSuccess: () => {
                      setLogged ? setLogged(false) : null;
                      navigate('/login');
                    },
                  });
                }
              }}
            >
              <Icon width={24} height={24} name='logout' />
            </Button>
          )}
        </div>
      </div>
    </header>
  );
});

export default Header;
