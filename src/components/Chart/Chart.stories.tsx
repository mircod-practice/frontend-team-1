import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Chart from './Chart';

export default {
  title: 'Chart',
  component: Chart,

  argTypes: {},
} as ComponentMeta<typeof Chart>;

const Template: ComponentStory<typeof Chart> = (args) => <Chart {...args} />;

export const Primary = Template.bind({});

Primary.args = {};
