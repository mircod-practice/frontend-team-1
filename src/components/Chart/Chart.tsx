/* eslint-disable camelcase */
import React, { FC } from 'react';
import style from './Chart.module.scss';
import {
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  Area,
  AreaChart,
} from 'recharts';
import { Measurement } from '../../store/services/api/Api';

type ChartProps = {
  measurements: Measurement[];
};

const Chart: FC<ChartProps> = ({ measurements }) => {
  let minTemp = 100;
  let maxTemp = 0;
  const [localMeasurements, setLocalMeasurements] =
    React.useState(measurements);

  React.useEffect(() => {
    setLocalMeasurements(
      measurements.map((m) => {
        return {
          ...m,
          measured_at: new Date(m.measured_at).toLocaleTimeString('ru-RU', {
            hour: '2-digit',
            minute: '2-digit',
          }),
        };
      }),
    );
  }, [measurements]);

  const ticksY = [35, 35.5, 36, 36.5, 37, 37.5, 38, 38.5, 39];

  const SetInterval = (measurements: Measurement[]) => {
    for (let i = 0; i < measurements.length; i++) {
      if (measurements[i].temperature > maxTemp) {
        maxTemp = measurements[i].temperature;
      }

      if (measurements[i].temperature < minTemp) {
        minTemp = measurements[i].temperature;
      }
    }
    // делаем значения целыми
    minTemp = Math.floor(minTemp);
    maxTemp = Math.ceil(maxTemp);

    if (minTemp < 35) {
      minTemp = minTemp - 1;
    } else {
      minTemp = 35;
    }

    if (maxTemp > 39) {
      maxTemp = maxTemp + 1;
    } else {
      maxTemp = 39;
    }
    if (minTemp == 35 && maxTemp == 39) {
      return true;
    } else {
      return false;
    }
  };
  const normal = SetInterval(localMeasurements);

  return (
    <div className={style.component}>
      <ResponsiveContainer>
        <AreaChart
          width={1240}
          height={363}
          data={localMeasurements}
          syncId='anyId'
        >
          <CartesianGrid stroke='#E4EAF0' horizontal={true} vertical={false} />
          <XAxis
            dataKey='measured_at'
            stroke='#525358'
            axisLine={false}
            tickLine={false}
            tickMargin={10}
          />
          <YAxis
            stroke='#525358'
            axisLine={false}
            tickLine={false}
            domain={[minTemp, maxTemp]}
            allowDataOverflow={normal}
            interval={0}
            tickCount={10}
            ticks={normal ? ticksY : undefined}
          />
          <Tooltip />
          <defs>
            <linearGradient id='splitColor'>
              <stop offset='0' stopColor='#85b0fa' stopOpacity='0.8' />
              <stop offset='0.25' stopColor='#b6aaef' stopOpacity='0.8' />
              <stop offset='0.5' stopColor='#eca5c5' stopOpacity='0.8' />
              <stop offset='0.75' stopColor='#ecb4a6' stopOpacity='0.8' />
              <stop offset='1' stopColor='#e3bda3' stopOpacity='0.8' />
            </linearGradient>

            <linearGradient id='strokeColor'>
              <stop offset='0' stopColor='#10A7C8' stopOpacity='0.6' />
              <stop offset='0.3' stopColor='#8585BA' stopOpacity='0.6' />
              <stop offset='1' stopColor='#F08C83' stopOpacity='0.6' />
            </linearGradient>
          </defs>
          <Area
            type='monotone'
            dataKey='temperature'
            stroke='url(#strokeColor)'
            strokeWidth={2}
            fill='url(#splitColor)'
          />
        </AreaChart>
      </ResponsiveContainer>
    </div>
  );
};

export default Chart;
