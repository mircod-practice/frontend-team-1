import React, { FC } from 'react';
import { ColorType } from '../../utils/ColorType';
import { Icon } from '../_ui/Icon/Icon';
import style from './MeasurementsTable.module.scss';
import cn from 'classnames';
import { DateParse } from '../../utils/DateParse';
import { User } from '../../store/services/api/Api';
import { CheckDateMeasurements } from '../../utils/CheckDateMeasurements';

type MeasurementsTableProps = {
  user: User;
  date: string;
  setDate: (data: string) => void;
  isEmpty: boolean;
};

const MeasurementsTable: FC<MeasurementsTableProps> = ({
  user,
  date,
  setDate,
  isEmpty,
}) => {
  return (
    <div className={style.component}>
      <div className={style.dateSwitch}>
        <button
          className={style.arrowIcon}
          onClick={() => {
            const tempDate = new Date(date.split('.').reverse().join('-'));
            tempDate.setDate(tempDate.getDate() - 1);
            setDate(tempDate.toLocaleDateString());
          }}
        >
          <Icon name={'keyboard_arrow_left_gray'} height={24} width={24} />
        </button>
        <div className={style.dateContent}>
          <div className={style.today}>
            <Icon name={'today'} height={24} width={24} />
            <p>{date}</p>
          </div>
        </div>

        <button
          className={style.arrowIcon}
          onClick={() => {
            const tempDate = new Date(date.split('.').reverse().join('-'));
            tempDate.setDate(tempDate.getDate() + 1);
            setDate(tempDate.toLocaleDateString());
          }}
        >
          <Icon name={'keyboard_arrow_right'} height={24} width={24} />
        </button>
      </div>
      {!isEmpty && (
        <table>
          <thead>
            <tr>
              <th>Дата</th>
              <th>Измерение</th>
              <th>Устройство</th>
            </tr>
          </thead>
          <tbody>
            {CheckDateMeasurements(date, user.measurements).map((m) => (
              <tr key={m.id}>
                <td>{DateParse(m.measured_at)}</td>
                <td className={cn(style[`${ColorType(m.temperature)}`])}>
                  {m.temperature}°C
                </td>
                <td>UID: {m.device}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default MeasurementsTable;
