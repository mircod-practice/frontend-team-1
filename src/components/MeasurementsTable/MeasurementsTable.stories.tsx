import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import MeasurementsTable from './MeasurementsTable';

export default {
  title: 'MeasurementsTable',
  component: MeasurementsTable,

  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof MeasurementsTable>;

const Template: ComponentStory<typeof MeasurementsTable> = (args) => (
  <MeasurementsTable {...args} />
);

export const Primary = Template.bind({});

Primary.args = {
  // primary: true,
  // label: 'MeasurementsTable',
};
