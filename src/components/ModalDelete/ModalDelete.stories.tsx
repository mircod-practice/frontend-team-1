import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import ModalDelete from './ModalDelete';

export default {
  title: 'ModalDelete',
  component: ModalDelete,

  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof ModalDelete>;

const Template: ComponentStory<typeof ModalDelete> = (args) => (
  <ModalDelete {...args} />
);

export const Primary = Template.bind({});

Primary.args = {};
