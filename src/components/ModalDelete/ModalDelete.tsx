import React from 'react';
import Modal from 'react-modal';
import style from './ModalDelete.module.scss';
import cn from 'classnames';
import { User } from '../../store/services/api/Api';
import { useUserDelete } from '../../store/services/api/user/useUserDelete';
import { Icon } from '../_ui/Icon/Icon';

const getDeleteText = (users: User[]) => {
  if (users.length === 1) {
    return `пользователя ${users[0].first_name} ${users[0].last_name}`;
  } else {
    return `${users.length} пользователей`;
  }
};

type ModalDeleteProps = {
  users: User[];
  isOpen: boolean;
  setIsOpen: (data: boolean) => void;
  setActive: (data: User[]) => void;
};

Modal.setAppElement('#root');

const ModalDelete: React.FC<ModalDeleteProps> = ({
  users,
  isOpen,
  setIsOpen,
  setActive,
}) => {
  const { mutate, isLoading } = useUserDelete('', '', 1);
  const [error, setError] = React.useState(false);

  return (
    <Modal
      className={cn(style.component, style.Modal)}
      overlayClassName={cn(style.component, style.Overlay)}
      isOpen={isOpen}
      onRequestClose={() => {
        setIsOpen(false);
      }}
    >
      <h2>Удаление пользователя</h2>

      <p>Вы уверены, что хотите удалить {getDeleteText(users)}?</p>
      <p>Действие нельзя будет отменить</p>
      {error && <p className={style.error}>Произошла ошибка</p>}

      <div className={cn(style.buttons)}>
        {!isLoading ? (
          <button
            className={cn(style.buttons, style.confirm)}
            onClick={async () => {
              mutate(
                { records: users.map((u) => u.id) },
                {
                  onError: () => setError(true),
                  onSuccess: () => {
                    setIsOpen(false);
                    setActive([]);
                  },
                },
              );
            }}
          >
            Подтвердить
          </button>
        ) : (
          <button
            disabled={isLoading}
            className={cn(style.buttons, style.confirm)}
          >
            <Icon name='loading' width={24} height={24}></Icon>
          </button>
        )}
        <button
          className={cn(style.buttons, style.go_back)}
          onClick={() => {
            setIsOpen(false);
          }}
        >
          Отменить
        </button>
      </div>
    </Modal>
  );
};

export default ModalDelete;
