import React from 'react';
import NoResults from '../../assets/not_found.png';
import style from './NoUsersFound.module.scss';

const NoUsersFound = () => {
  return (
    <div className={style.component}>
      <img src={NoResults} alt='No results' />
      <p>Пока здесь нет ни одного пользователя</p>
    </div>
  );
};

export default NoUsersFound;
