import React from 'react';
import { Icon } from '../_ui/Icon/Icon';
import style from './UsersTable.module.scss';
import TableRow from '../_ui/TableRow/TableRow';
import cn from 'classnames';
import { User } from '../../store/services/api/Api';
import { TemperatureFilter } from '../_ui/TemperatureFilter/TemperatureFilter';

type UsersTableProps = {
  active: User[];
  setActive: (data: User[]) => void;
  data: User[] | undefined;
  isLoading: boolean;
  filters: {
    normal: boolean;
    high: boolean;
    low: boolean;
  };
  setFilters: (data: { normal: boolean; high: boolean; low: boolean }) => void;
};

const UsersTable: React.FC<UsersTableProps> = ({
  active,
  setActive,
  data,
  isLoading,
  filters,
  setFilters,
}) => {
  const [checkedAll, setCheckedAll] = React.useState(false);

  React.useEffect(() => {
    if (active.length === 0) {
      setCheckedAll(false);
    } else if (active.length === data?.length) {
      setCheckedAll(true);
    } else {
      setCheckedAll(false);
    }
  }, [active]);

  return (
    <div className={style.component}>
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>ФИО или табельный номер</th>
            <th>
              Температура{' '}
              <TemperatureFilter filters={filters} setFilters={setFilters} />
            </th>
            <th>UID устройства</th>
            <th>Заряд</th>
            <th className={cn({ [style.checked]: checkedAll })}>
              {!checkedAll ? (
                <Icon
                  name='check_box_outline_blank'
                  width={24}
                  height={24}
                  onClick={() => {
                    setCheckedAll(true);
                    setActive(data || []);
                  }}
                ></Icon>
              ) : (
                <Icon
                  name='check_box'
                  width={24}
                  height={24}
                  onClick={() => {
                    setCheckedAll(false);
                    setActive([]);
                  }}
                ></Icon>
              )}
            </th>
          </tr>
        </thead>
        {isLoading ? (
          <tbody>
            {[...Array(10)].map((_, id) => (
              <TableRow
                id={id}
                key={id}
                user={null}
                active={active}
                setActive={setActive}
                isLoading={true}
                checkedAll={checkedAll}
              ></TableRow>
            ))}
          </tbody>
        ) : (
          <tbody>
            {data?.map((user, id) => (
              <TableRow
                id={id}
                user={user}
                key={user.id}
                active={active}
                setActive={setActive}
                isLoading={false}
                checkedAll={checkedAll}
              ></TableRow>
            ))}
          </tbody>
        )}
      </table>
    </div>
  );
};

export default UsersTable;
