import React from 'react';
import { Button } from '../../components/_ui/Button/Button';
import { Icon } from '../../components/_ui/Icon/Icon';
import { Formik, Form, Field } from 'formik';
import { useAuth } from '../../store/services/api/auth/useAuth';
import style from './LoginForm.module.scss';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../../store/AuthContext/AuthContext';

const LoginSchema = Yup.object().shape({
  username: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

const LoginForm: React.FC = () => {
  const [aggree, setAggree] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const [error, setError] = React.useState(false);
  const { setLogged } = React.useContext(AuthContext);
  const { mutate } = useAuth();
  const navigate = useNavigate();

  return (
    <Formik
      initialValues={{ username: '', password: '' }}
      validationSchema={LoginSchema}
      onSubmit={(values, { setSubmitting, setTouched }) => {
        mutate(values, {
          onError: () => {
            setError(true);
            setTouched({ username: false, password: false });
          },
          onSuccess: () => {
            setLogged ? setLogged(true) : null;
            navigate('/');
          },
        });
        setSubmitting(false);
      }}
      validateOnBlur={false}
      validateOnChange={false}
    >
      {({ isSubmitting, values, touched }) => (
        <Form className={style.component}>
          <label htmlFor='username'>Имя пользователя</label>
          <Field type='text' name='username' disabled={isSubmitting} />

          <label htmlFor='password'>Пароль</label>
          <div className={style.input_container}>
            <Field
              className={style.password}
              type={showPassword ? 'text' : 'password'}
              name='password'
              disabled={isSubmitting}
            />
            {showPassword ? (
              <Icon
                width={24}
                height={24}
                name='visibility'
                onClick={() => setShowPassword(!showPassword)}
              />
            ) : (
              <Icon
                width={24}
                height={24}
                name='visibility_off'
                onClick={() => setShowPassword(!showPassword)}
              />
            )}
          </div>

          <span className={style.aggree}>
            {aggree ? (
              <Icon
                width={20}
                height={20}
                name='check_box'
                onClick={() => setAggree(!aggree)}
              />
            ) : (
              <Icon
                width={20}
                height={20}
                name='check_box_outline_blank'
                onClick={() => setAggree(!aggree)}
              />
            )}
            Соглашаюсь с&nbsp;<a href='#'>Условиями использования</a>
          </span>

          {error ? (
            !touched.username || !touched.password ? (
              <span className={style.error}>
                Неверное имя пользователя или пароль
              </span>
            ) : null
          ) : null}

          <Button
            buttonType='login'
            disabled={
              isSubmitting ||
              !aggree ||
              values.username == '' ||
              values.password == ''
            }
            onClick={() => console.log('submit')}
          >
            {isSubmitting ? (
              <Icon width={24} height={24} name='loading' />
            ) : (
              'Войти'
            )}
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default LoginForm;
