import { useMutation } from 'react-query';
import { api } from '../Api';

export const useVerify = () => {
  return useMutation(async (token: string) => {
    const data = { token: token };
    const res = await api.auth.authTokenVerifyCreate(data);
    return res;
  });
};
