import storage from 'local-storage-fallback';
import { useMutation } from 'react-query';
import { api } from '../Api';

export const useLogout = () => {
  return useMutation(async (refresh: string) => {
    const data = { refresh: refresh };
    const res = await api.auth.authLogoutCreate(data);
    if (res.status === 204) {
      storage.removeItem('access_token');
      storage.removeItem('refresh_token');
    }
    return res;
  });
};
