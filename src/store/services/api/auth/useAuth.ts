import storage from 'local-storage-fallback';
import { useMutation } from 'react-query';
import { api } from '../Api';
import { AuthProps } from './interfaces';

export const useAuth = () => {
  return useMutation(async (data: AuthProps) => {
    const res = await api.auth.authSigninCreate(data);
    if (res.data.access) {
      storage.setItem('access_token', res.data.access);
    }
    if (res.data.refresh) {
      storage.setItem('refresh_token', res.data.refresh);
    }
    return res;
  });
};
