/* eslint-disable camelcase */
import { useQuery } from 'react-query';
import { api } from '../Api';

type UserListProps = {
  search?: string;
  ordering?: string;
  page: number;
  last_temperature__lte?: number;
  last_temperature__gte?: number;
};

export const useUserList = ({
  search = '',
  ordering = '',
  page,
  last_temperature__lte,
  last_temperature__gte,
}: UserListProps) => {
  return useQuery({
    queryKey: ['UserList', search, ordering, page],
    queryFn: async () =>
      await api.user.userList({
        search,
        ordering,
        page,
        last_temperature__gte,
        last_temperature__lte,
      }),
    cacheTime: Infinity,
    staleTime: Infinity,
    keepPreviousData: true,
  });
};
