import { useMutation, useQueryClient } from 'react-query';
import { api, User } from '../Api';

type useUserUpdateProps = {
  id: number;
  user: User;
};

export const useUserUpdate = (id: number) => {
  const queryClient = useQueryClient();

  return useMutation(
    async ({ id, user }: useUserUpdateProps) => {
      const res = await api.user.userPartialUpdate(id, user);
      return res;
    },
    {
      onMutate: async (User) => {
        await queryClient.cancelQueries(['User', id]);

        const snapshot = queryClient.getQueryData(['User', id]);

        queryClient.setQueryData(
          ['User', id],
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (oldData: any) => {
            return {
              ...oldData,
              data: {
                ...oldData.data,
                user: { ...oldData.data.user, ...User },
              },
            };
          },
        );

        return snapshot;
      },
      onError: (error, User, snapshot) => {
        queryClient.setQueryData(['User', id], snapshot);
      },

      onSuccess: () => queryClient.invalidateQueries(['User']),
    },
  );
};
