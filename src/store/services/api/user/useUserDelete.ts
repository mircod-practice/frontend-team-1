import { useMutation, useQueryClient } from 'react-query';
import { api, BulkDelete, User } from '../Api';

export const useUserDelete = (
  search: string,
  ordering: string,
  page: number,
) => {
  const queryClient = useQueryClient();

  return useMutation(
    async (data: BulkDelete) => {
      const res = await api.user.userBulkDeleteCreate(data);
      return res;
    },
    {
      onMutate: async (DeletedUsers) => {
        await queryClient.cancelQueries(['UserList', search, ordering, page]);

        const snapshot = queryClient.getQueryData([
          'UserList',
          search,
          ordering,
          page,
        ]);

        queryClient.setQueryData(
          ['UserList', search, ordering, page],
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          (oldData: any) => {
            return {
              ...oldData,
              data: {
                ...oldData.data,
                count: oldData.data.count - DeletedUsers.records.length,
                results: oldData.data.results.filter((u: User) => {
                  !DeletedUsers.records.includes(u.id);
                }),
              },
            };
          },
        );

        return snapshot;
      },

      onError: (error, DeletedUsers, snapshot) => {
        queryClient.setQueryData(
          ['UserList', search, ordering, page],
          snapshot,
        );
      },

      onSuccess: () => queryClient.invalidateQueries(['UserList']),
    },
  );
};
