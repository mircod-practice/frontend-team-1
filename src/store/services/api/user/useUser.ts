import { useQuery } from 'react-query';
import { api } from '../Api';

type UserProps = {
  id: number;
};

export const useUser = ({ id }: UserProps) => {
  return useQuery({
    queryKey: ['User', id],
    queryFn: async () => await api.user.userRead(id),
    cacheTime: Infinity,
    staleTime: Infinity,
    keepPreviousData: true,
    refetchInterval: 30000,
    refetchIntervalInBackground: true,
  });
};
