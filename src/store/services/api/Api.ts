/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Logout {
  /** Refresh */
  refresh: string;
}

export type TokenBlacklistResponse = object;

export interface MyTokenObtainPair {
  /** Username */
  username: string;

  /** Password */
  password: string;
}

export interface TokenObtainPairResponse {
  /** Access */
  access: string;

  /** Refresh */
  refresh: string;
}

export interface Registration {
  /** ID */
  id?: number;

  /** Username */
  username: string;

  /** First name */
  first_name: string;

  /** Last name */
  last_name: string;

  /** Password */
  password: string;
}

export interface TokenRefresh {
  /** Refresh */
  refresh: string;

  /** Access */
  access?: string;
}

export interface TokenRefreshResponse {
  /** Access */
  access: string;
}

export interface TokenVerify {
  /** Token */
  token: string;
}

export type TokenVerifyResponse = object;

export interface Device {
  /** Uuid */
  uuid: string;

  /** Is active */
  is_active?: boolean;

  /**
   * Charge
   * @min 0
   * @max 100
   */
  charge: number;
}

export interface Measurement {
  /** ID */
  id: number;

  /** Temperature */
  temperature: number;

  /**
   * Measured at
   * @format date-time
   */
  measured_at: string;

  /** Device */
  device?: string;

  /** User */
  user?: number;
}

export interface User {
  /** ID */
  id: number;

  /** Username */
  username: string;

  /** First name */
  first_name: string;

  /** Last name */
  last_name: string;
  device?: Device;

  /** Last temperature */
  last_temperature: number | null;
  measurements: Measurement[];

  /** Role */
  role: 'medic' | 'patient';
}

export interface BulkDelete {
  records: number[];
}

import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  ResponseType,
} from 'axios';
import storage from 'local-storage-fallback';

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams
  extends Omit<AxiosRequestConfig, 'data' | 'params' | 'url' | 'responseType'> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<
  FullRequestParams,
  'body' | 'method' | 'query' | 'path'
>;

export interface ApiConfig<SecurityDataType = unknown>
  extends Omit<AxiosRequestConfig, 'data' | 'cancelToken'> {
  securityWorker?: (
    securityData: SecurityDataType | null,
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = 'application/json',
  FormData = 'multipart/form-data',
  UrlEncoded = 'application/x-www-form-urlencoded',
}

export class HttpClient<SecurityDataType = unknown> {
  public instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>['securityWorker'];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({
    securityWorker,
    secure,
    format,
    ...axiosConfig
  }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({
      ...axiosConfig,
      baseURL: axiosConfig.baseURL || 'http://3.127.38.160/api/v1/',
    });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private mergeRequestParams(
    params1: AxiosRequestConfig,
    params2?: AxiosRequestConfig,
  ): AxiosRequestConfig {
    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.instance.defaults.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createFormData(input: Record<string, unknown>): FormData {
    return Object.keys(input || {}).reduce((formData, key) => {
      const property = input[key];
      formData.append(
        key,
        property instanceof Blob
          ? property
          : typeof property === 'object' && property !== null
          ? JSON.stringify(property)
          : `${property}`,
      );
      return formData;
    }, new FormData());
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === 'boolean' ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = (format && this.format) || void 0;

    if (
      type === ContentType.FormData &&
      body &&
      body !== null &&
      typeof body === 'object'
    ) {
      requestParams.headers.common = { Accept: '*/*' };
      requestParams.headers.post = {};
      requestParams.headers.put = {};

      body = this.createFormData(body as Record<string, unknown>);
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData
          ? { 'Content-Type': type }
          : {}),
        ...(requestParams.headers || {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
    });
  };
}

/**
 * @title Snippets API
 * @version v1
 * @license BSD License
 * @termsOfService https://www.google.com/policies/terms/
 * @baseUrl http://3.127.38.160/api/v1/
 * @contact <contact@snippets.local>
 *
 * Test description
 */
export class Api<
  SecurityDataType extends unknown,
> extends HttpClient<SecurityDataType> {
  auth = {
    /**
     * No description
     *
     * @tags auth
     * @name AuthLogoutCreate
     * @request POST:/auth/logout/
     * @secure
     */
    authLogoutCreate: (data: Logout, params: RequestParams = {}) =>
      this.request<TokenBlacklistResponse, any>({
        path: `/auth/logout/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags auth
     * @name AuthSigninCreate
     * @request POST:/auth/signin/
     * @secure
     */
    authSigninCreate: (data: MyTokenObtainPair, params: RequestParams = {}) =>
      this.request<TokenObtainPairResponse, any>({
        path: `/auth/signin/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags auth
     * @name AuthSignupCreate
     * @request POST:/auth/signup/
     * @secure
     */
    authSignupCreate: (data: Registration, params: RequestParams = {}) =>
      this.request<Registration, any>({
        path: `/auth/signup/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags auth
     * @name AuthTokenRefreshCreate
     * @request POST:/auth/token/refresh/
     * @secure
     */
    authTokenRefreshCreate: (data: TokenRefresh, params: RequestParams = {}) =>
      this.request<TokenRefreshResponse, any>({
        path: `/auth/token/refresh/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags auth
     * @name AuthTokenVerifyCreate
     * @request POST:/auth/token/verify/
     * @secure
     */
    authTokenVerifyCreate: (data: TokenVerify, params: RequestParams = {}) =>
      this.request<TokenVerifyResponse, any>({
        path: `/auth/token/verify/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  device = {
    /**
     * No description
     *
     * @tags device
     * @name DeviceCreate
     * @request POST:/device/
     * @secure
     */
    deviceCreate: (data: Device, params: RequestParams = {}) =>
      this.request<Device, any>({
        path: `/device/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  temp = {
    /**
     * No description
     *
     * @tags temp
     * @name TempCreate
     * @request POST:/temp/
     * @secure
     */
    tempCreate: (data: Measurement, params: RequestParams = {}) =>
      this.request<Measurement, any>({
        path: `/temp/`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),
  };
  user = {
    /**
     * No description
     *
     * @tags user
     * @name UserList
     * @request GET:/user/
     * @secure
     */
    userList: (
      query?: {
        search?: string;
        ordering?: string;
        last_temperature__lte?: number;
        last_temperature__gte?: number;
        page?: number;
      },
      params: RequestParams = {},
    ) =>
      this.request<
        {
          count: number;
          next?: string | null;
          previous?: string | null;
          results: User[];
        },
        any
      >({
        path: `/user/`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags user
     * @name UserBulkDeleteCreate
     * @request POST:/user/bulk-delete
     * @secure
     */
    userBulkDeleteCreate: (data: BulkDelete, params: RequestParams = {}) =>
      this.request<BulkDelete, any>({
        path: `/user/bulk-delete`,
        method: 'POST',
        body: data,
        secure: true,
        type: ContentType.Json,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags user
     * @name UserCurrentList
     * @request GET:/user/current
     * @secure
     */
    userCurrentList: (query?: { page?: number }, params: RequestParams = {}) =>
      this.request<
        {
          count: number;
          next?: string | null;
          previous?: string | null;
          results: User[];
        },
        any
      >({
        path: `/user/current`,
        method: 'GET',
        query: query,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags user
     * @name UserRead
     * @request GET:/user/{id}/
     * @secure
     */
    userRead: (id: number, params: RequestParams = {}) =>
      this.request<{ user: User }, any>({
        path: `/user/${id}/`,
        method: 'GET',
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags user
     * @name UserUpdate
     * @request PUT:/user/{id}/
     * @secure
     */
    userUpdate: (id: number, data: User, params: RequestParams = {}) =>
      this.request<User, any>({
        path: `/user/${id}/`,
        method: 'PUT',
        body: data,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags user
     * @name UserPartialUpdate
     * @request PATCH:/user/{id}/
     * @secure
     */
    userPartialUpdate: (id: number, data: User, params: RequestParams = {}) =>
      this.request<User, any>({
        path: `/user/${id}/`,
        method: 'PATCH',
        body: data,
        secure: true,
        format: 'json',
        ...params,
      }),

    /**
     * No description
     *
     * @tags user
     * @name UserDelete
     * @request DELETE:/user/{id}/
     * @secure
     */
    userDelete: (id: number, params: RequestParams = {}) =>
      this.request<void, any>({
        path: `/user/${id}/`,
        method: 'DELETE',
        secure: true,
        ...params,
      }),
  };
}

export const api = new Api();
api.instance.interceptors.request.use(async (config) => {
  const storageToken = storage.getItem('access_token');

  if (storageToken && storageToken.length !== 0) {
    config.headers.Authorization = 'Bearer ' + storageToken;
  }

  return config;
});
