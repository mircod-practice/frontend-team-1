import React from 'react';

interface AuthContextProps {
  logged: boolean;
  setLogged?: (data: boolean) => void;
}

interface AuthProviderProps {
  children: React.ReactElement;
}

export const AuthContext = React.createContext<AuthContextProps>({
  logged: false,
});

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [logged, setLogged] = React.useState(false);

  return (
    <AuthContext.Provider value={{ logged, setLogged }}>
      {children}
    </AuthContext.Provider>
  );
};
