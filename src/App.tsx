import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { Route, Routes } from 'react-router-dom';
import Header from './components/Header/Header';
import UserList from './pages/UserList/UserList';
import { WithAuth } from './utils/WithAuth/WithAuth';
import Auth from './pages/Auth/Auth';
import './App.scss';
import { AuthProvider } from './store/AuthContext/AuthContext';
import NotFound from './pages/NotFound/NotFound';
import UserPage from './pages/UserPage/UserPage';

const queryClient = new QueryClient();

function App() {
  return (
    <AuthProvider>
      <QueryClientProvider client={queryClient}>
        <Header />
        <div className='content'>
          <Routes>
            <Route
              path='/'
              element={
                <WithAuth>
                  <UserList />
                </WithAuth>
              }
            ></Route>
            <Route
              path='/user/:id'
              element={
                <WithAuth>
                  <UserPage />
                </WithAuth>
              }
            ></Route>
            <Route path='/login' element={<Auth />}></Route>
            <Route path='*' element={<NotFound />}></Route>
          </Routes>
        </div>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </AuthProvider>
  );
}

export default App;
