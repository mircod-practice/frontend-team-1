import { Measurement } from '../store/services/api/Api';

export const CheckDateMeasurements = (
  date: string,
  measurements: Measurement[],
) => {
  return measurements.filter(
    (m) => new Date(m.measured_at).toLocaleDateString() === date,
  );
};
