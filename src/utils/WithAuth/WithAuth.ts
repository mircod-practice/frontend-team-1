import storage from 'local-storage-fallback';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../../store/AuthContext/AuthContext';
import { useVerify } from '../../store/services/api/auth/useVerify';

type withAuthProps = {
  children: React.ReactElement;
};

export const WithAuth: React.FC<withAuthProps> = ({ children }) => {
  const { mutate } = useVerify();
  const token = storage.getItem('access_token');
  const { logged, setLogged } = React.useContext(AuthContext);
  const navigate = useNavigate();

  React.useEffect(() => {
    if (token && token !== '') {
      mutate(token, {
        onError: () => {
          setLogged ? setLogged(false) : null;
          navigate('/login');
        },
        onSuccess: () => {
          setLogged ? setLogged(true) : null;
        },
      });
    } else {
      navigate('/login');
    }
  }, []);

  if (logged) {
    return children;
  } else {
    return null;
  }
};
