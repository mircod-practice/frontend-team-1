export const GetChargeColor = (charge: number) => {
  if (charge >= 90) {
    return '#84C299';
  } else if (charge > 15) {
    return '#5CA5FA';
  } else {
    return '#E38888';
  }
};
