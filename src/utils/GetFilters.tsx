export const GetFilters = (filters: {
  normal: boolean;
  high: boolean;
  low: boolean;
}) => {
  const { normal, high, low } = filters;
  if (normal === true && high === true && low === true) {
    return <b>все</b>;
  }
  if (normal === true && high === false && low === false) {
    return (
      <>
        температура <b>нормальная</b>
      </>
    );
  }
  if (normal === false && high === true && low === false) {
    return (
      <>
        температура <b>высокая</b>
      </>
    );
  }
  if (normal === false && high === false && low === true) {
    return (
      <>
        температура <b>низкая</b>
      </>
    );
  }
  if (normal === true && high === true && low === false) {
    return (
      <>
        температура <b>нормальная или высокая</b>
      </>
    );
  }
  if (normal === false && high === true && low === true) {
    return (
      <>
        температура <b>высокая или низкая</b>
      </>
    );
  }
  if (normal === true && high === false && low === true) {
    return (
      <>
        температура <b>нормальная или низкая</b>
      </>
    );
  }
  return '';
};
