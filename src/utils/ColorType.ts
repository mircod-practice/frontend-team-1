export const ColorType = (temperature: number | undefined) => {
  let tempcolor: string;
  temperature = temperature ? temperature : 36.6;
  if (temperature < 36) {
    tempcolor = 'low';
  } else if (temperature >= 37) {
    tempcolor = 'high';
  } else {
    tempcolor = 'normal';
  }
  return tempcolor;
};
