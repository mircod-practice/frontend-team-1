export const DateParse = (date: string | undefined) => {
  const tempDate = new Date(date || '');
  const NewDate = tempDate.toLocaleDateString('ru-RU', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  });
  return NewDate;
};
