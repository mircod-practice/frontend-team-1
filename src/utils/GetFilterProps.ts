export const GetFilterProps = (filters: {
  normal: boolean;
  high: boolean;
  low: boolean;
}) => {
  const { normal, high, low } = filters;
  if (normal === true && high === true && low === true) {
    return {
      lte: undefined,
      gte: undefined,
    };
  }
  if (normal === false && high === false && low === false) {
    return {
      lte: undefined,
      gte: undefined,
    };
  }
  if (normal === true && high === false && low === false) {
    return {
      lte: 37,
      gte: 35.9,
    };
  }
  if (normal === false && high === true && low === false) {
    return {
      lte: undefined,
      gte: 36.9,
    };
  }
  if (normal === false && high === false && low === true) {
    return {
      lte: 36,
      gte: undefined,
    };
  }
  if (normal === true && high === true && low === false) {
    return {
      lte: undefined,
      gte: 35.9,
    };
  }
  if (normal === false && high === true && low === true) {
    return {
      lte: 36,
      gte: 36.9,
    };
  }
  if (normal === true && high === false && low === true) {
    return {
      lte: 37,
      gte: undefined,
    };
  }
  return {
    lte: undefined,
    gte: undefined,
  };
};
