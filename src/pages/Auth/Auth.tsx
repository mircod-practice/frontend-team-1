import React from 'react';
import logo from '../../assets/logo.svg';
import style from './Auth.module.scss';
import LoginForm from '../../components/LoginForm/LoginForm';
import { useVerify } from '../../store/services/api/auth/useVerify';
import storage from 'local-storage-fallback';
import { useNavigate } from 'react-router-dom';

const Auth: React.FC = () => {
  const { mutate } = useVerify();
  const token = storage.getItem('access_token');
  const [tokenCheck, setTokenCheck] = React.useState(true);
  const navigate = useNavigate();

  React.useEffect(() => {
    if (token && token !== '') {
      mutate(token, {
        onSuccess: () => {
          navigate('/');
        },
        onError: () => {
          setTokenCheck(false);
        },
      });
    } else {
      setTokenCheck(false);
    }
  }, []);

  if (!tokenCheck) {
    return (
      <div className={style.component}>
        <div className={style.content}>
          <img src={logo} alt='logo' />
          <h2>Авторизация</h2>
          <LoginForm></LoginForm>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default Auth;
