/* eslint-disable camelcase */
import React from 'react';
import ControlPanel from '../../components/ControlPanel/ControlPanel';
import NoUsersFound from '../../components/NoUsersFound/NoUsersFound';
import UsersTable from '../../components/UsersTable/UsersTable';
import TableBottom from '../../components/_ui/TableBottom/TableBottom';
import { User } from '../../store/services/api/Api';
import { useUserList } from '../../store/services/api/user/useUserList';
import { GetFilterProps } from '../../utils/GetFilterProps';

const UserList: React.FC = () => {
  const [page, setPage] = React.useState(0);
  const [searchValue, setSearchValue] = React.useState('');
  const [active, setActive] = React.useState([] as Array<User>);
  const [filters, setFilters] = React.useState({
    normal: false,
    high: false,
    low: false,
  });

  const { isLoading, data, isPreviousData, refetch } = useUserList({
    search: searchValue,
    page: page + 1,
    last_temperature__lte: GetFilterProps(filters).lte,
    last_temperature__gte: GetFilterProps(filters).gte,
  });

  React.useEffect(() => {
    refetch();
  }, [filters]);

  return (
    <div>
      <ControlPanel
        active={active}
        setSearchValue={setSearchValue}
        data={data?.data.results}
        isLoading={isLoading}
        setActive={setActive}
      />
      {!isLoading &&
      data &&
      data.data.results.length === 0 &&
      !filters.normal &&
      !filters.high &&
      !filters.low ? (
        <NoUsersFound />
      ) : (
        <UsersTable
          active={active}
          setActive={setActive}
          isLoading={isLoading}
          data={data?.data.results}
          filters={filters}
          setFilters={setFilters}
        />
      )}
      {isLoading ? null : data && data.data.results.length !== 0 ? (
        <TableBottom
          active={active}
          setActive={setActive}
          filters={filters}
          setFilters={setFilters}
          page={page}
          setPage={setPage}
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          count={data!.data.count}
          isPreviousData={isPreviousData}
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          hasMore={!!data!.data.next}
        ></TableBottom>
      ) : null}
    </div>
  );
};

export default UserList;
