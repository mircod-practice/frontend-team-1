import style from './NotFound.module.scss';
import notFound from '../../assets/not_found.png';
import { Button } from '../../components/_ui/Button/Button';
import { Icon } from '../../components/_ui/Icon/Icon';
import { useNavigate } from 'react-router-dom';

const NotFound = () => {
  const navigate = useNavigate();
  return (
    <div className={style.component}>
      <div className={style.content}>
        <img src={notFound} alt='not_found' />
        <div className={style.text}>
          <h2>СТРАНИЦА НЕ НАЙДЕНА</h2>
          <p>Контент, который вы ищете, не существует.</p>
          <p>Его либо удалили, либо вы неправильно ввели ссылку.</p>
        </div>
        <div className={style.button}>
          <Button buttonType='go_back' onClick={() => navigate(-1)}>
            <Icon width={24} height={24} name='east' />
            Назад
          </Button>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
