import React from 'react';
import { useParams } from 'react-router-dom';
import Chart from '../../components/Chart/Chart';
import MeasurementsTable from '../../components/MeasurementsTable/MeasurementsTable';
import NoMeasurementsFound from '../../components/NoMeasurementsFound/NoMeasurementsFound';
import UserInfo from '../../components/UserInfo/UserInfo';
import { useUser } from '../../store/services/api/user/useUser';
import { CheckDateMeasurements } from '../../utils/CheckDateMeasurements';

const UserPage = () => {
  const params = useParams();
  const [date, setDate] = React.useState(
    new Date().toLocaleDateString('ru-RU', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
    }),
  );
  const { data, isLoading, isError } = useUser({
    id: Number(params.id),
  });

  if (isError) {
    return <div>Произошла ошибка!</div>;
  }

  if (
    !isLoading &&
    data &&
    CheckDateMeasurements(date, data.data.user.measurements).length !== 0
  ) {
    return (
      <div>
        <UserInfo user={data.data.user}></UserInfo>
        <Chart
          measurements={CheckDateMeasurements(
            date,
            data.data.user.measurements,
          ).reverse()}
        ></Chart>
        <MeasurementsTable
          user={data.data.user}
          date={date}
          setDate={setDate}
          isEmpty={false}
        ></MeasurementsTable>
      </div>
    );
  } else if (
    !isLoading &&
    data &&
    CheckDateMeasurements(date, data.data.user.measurements).length === 0
  ) {
    return (
      <div>
        <UserInfo user={data.data.user}></UserInfo>
        <NoMeasurementsFound></NoMeasurementsFound>
        <MeasurementsTable
          user={data.data.user}
          date={date}
          setDate={setDate}
          isEmpty={true}
        ></MeasurementsTable>
      </div>
    );
  } else {
    return null;
  }
};

export default UserPage;
